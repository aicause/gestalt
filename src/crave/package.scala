/**
 * Created by keith on 16/09/15.
 */
package object crave {

  // Types
  val TypeDisplay = "display"
  val TypeEarth = "earth"
  val TypeAlarm = "alarm"


  // Devices
  type Device = String
  val DeviceGovLab = "govlab"
  val DeviceMiniWall = "miniwall"

  //private val tileArraySizeGovLab = (7,3)
  private val tileArraySizeGovLab = (14,3)   // Hack: To do two windows per monitor

  // GovLab Tiling
  //private
  def tileLayoutGovLab(numberOfTiles: Int): List[Int] =
  {
    // Hack: For two windows per monitor
    numberOfTiles match {
      case n if  0 to  11 contains n => List (1, 2, 3, 4, 5, 6,  15, 16, 17, 18, 19, 20)
      case n if 12 to 16 contains n => List (1, 2, 3, 4, 5, 6, 7, 8,  15, 16, 17, 18, 19, 20, 21, 22,   29, 30, 31, 32, 33, 34, 35, 36)
      case _ => throw new IllegalArgumentException ("Cannot display more than 24 tiles")

      //        numberOfTiles match {
//          case n if  0 to  9 contains n => List (1, 2, 3, 8, 9, 10, 15, 16, 17)
//          case n if 10 to 12 contains n => List (1, 2, 3, 4, 8, 9, 10, 11, 15, 16, 17, 18)
//          case n if 13 to 15 contains n => List (1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 15, 16, 17, 18, 19)
//          case n if 16 to 18 contains n => List (1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20)
//          case n if 19 to 21 contains n => List (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21)
//        case _ => throw new IllegalArgumentException ("Cannot display more than 21 tiles")
      }
  }

  def pixelPositionOfContentInTiledLayout
  (
    horizontalTilingHop: Int = 1920,
    verticalTilingHop: Int = 1080,
    horizontalTilingOffset: Int = 0,
    verticalTilingOffset: Int = 0,
    numberOfTiles: Int
    )(contentIndex: Int): (Int, Int) =
  {
    val tileID = tileLayoutGovLab(numberOfTiles)(contentIndex)
    // TODO: This tile allocation should be generic not specific to GovLab
    //       We should pass in target display device and then pick a predefined tiling allocation for that device.

    val tileIndex = tileID-1

    val x: Int = tileIndex % tileArraySizeGovLab._1 * horizontalTilingHop + horizontalTilingOffset
    val y: Int = Math.floor(tileIndex / tileArraySizeGovLab._1).toInt * verticalTilingHop + verticalTilingOffset

    (x, y)
  }


  // --------------------------------------------------------------------------------- Severity
  type Severity = AbstractSeverity

  abstract class AbstractSeverity
  case object SeverityLow extends AbstractSeverity    { override def toString = "low" }
  case object SeverityMedium extends AbstractSeverity { override def toString = "medium" }
  case object SeverityHigh extends AbstractSeverity   { override def toString = "high" }






}
