package crave

/**
 * Created by keith on 7/09/15.
 */



// --------------------------------------------------------------------------------- CRAVE Command

abstract
class Command(val commandType: String)(val device: String)
{
  override def toString: String = s"<command $allAttributes></command>"

  protected lazy val allAttributes = standardAttributes + " " + attributes

  protected val standardAttributes = s"device='$device' type='$commandType'"

  protected val attributes: String
}

