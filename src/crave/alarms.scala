package crave

import core._

/**
 * Created by keith on 16/09/15.
 */
object alarms {

  abstract
  class Alarm(val severity: Severity)
             (val summary: String)
             (val contributors: List[String] = List())
             (val rootCause: String = "Unknown")
             (val relevantContent: List[Content] = List())
             (override val device: Device)
             (val location: GeoLocation)
             (val target: String)
    extends Command(TypeAlarm)(device)
  {
    override protected val attributes: String = s"severity='$severity'"

    override def toString: String =
    {
      // ----------------------------------------- Content

      val targetElement           = s"\n   <target>$target</target>"
      val summaryElement          = s"\n   <summary>$summary</summary>"

      // Cause
      val contributorsElement     = if (contributors.isEmpty) "" else contributors map { c => s"\n      <contributor>$c</contributor>" } mkString
      val rootCauseElement        = if (rootCause.isEmpty) "" else s"\n      <root>$rootCause</root>"
      val causeElement            =
        s"""
           |   <cause>$contributorsElement$rootCauseElement
           |   </cause>""".stripMargin

      // Relevance
      val contentElement          = relevantContent mkString ""
      val relevanceElement        = if (relevantContent.isEmpty) "" else s"\n   <relevance>$contentElement\n   </relevance>"

      val content = s"\n<content $allAttributes>" + targetElement + summaryElement + causeElement + relevanceElement + "\n</content>"


      // ----------------------------------------- Presentations

      // -------------------- GovLab
//      val command = s"\n<command $allAttributes>" + targetElement + summaryElement + causeElement + relevanceElement + "\n</command>"
//      val imageList = ContentImageListPositioned(relevantContent filter { _.isInstanceOf[ContentUriImageOverlay] } )
//      val otherList = ContentTextListMedium("Relevant Information:", relevantContent filter { ! _.isInstanceOf[ContentUriImageOverlay] } )

      // TODO: IMPORTANT Put the above code back after the ABB Demo
      // HACK: OLD XML Format for Lasith in ABB Demo
      val command = s"\n<command $allAttributes>" + targetElement + summaryElement + causeElement + relevanceElement + "\n</command>"
      val imageList = ContentImageListSimple(relevantContent filter { _.isInstanceOf[ContentUriImagePng] } )
      val otherList = ContentTextListMedium("Relevant Information:", relevantContent filter { ! _.isInstanceOf[ContentUriImageOverlay] } )

      // Texts
      val textAndContent = List(
        ContentTextTitle("SmartSpace Alarm"),
        ContentTextLarge(summary),
        ContentTextMedium(s"Target: $target"),
        ContentTextMedium(s"Root Cause: $rootCause"),

        // Contributors
        s"\n<text size='medium'>Contributing factors:</text>",
        contributors map { c => ContentTextMedium(c) } mkString "",

        otherList,
        imageList

      ) mkString ""

      val presentation =
        s"""
           |<presentation device='govlab'>$textAndContent
           |</presentation>""".stripMargin

      return content + presentation
    }

  }


  // TODO-IDEA: Use a builder ???
  abstract
  class AlarmHigh(override val summary: String)
                 (override val contributors: List[String] = List())
                 (override val rootCause: String = "Unknown")
                 (override val relevantContent: List[Content] = List())
                 (override val location: GeoLocation) (device: Device)
                 (override val target: String)
    extends Alarm(SeverityHigh)(summary)(contributors)(rootCause)(relevantContent)(device)(location)(target)



}
