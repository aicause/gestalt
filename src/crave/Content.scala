package crave

/**
 * Created by keith on 16/09/15.
 */
import core._

abstract class Content(val attributes: Map[String,String] = Map())
{
  protected
  val xmlAttributes = attributes map { kv => s"${kv._1}='${kv._2}'" } mkString " "

  def text: String = xmlAttributes.toString

  def updated(moreAttributes: Map[String, String]): Content
}

// ------------------------------------ Direct Content

class ContentText(override val text: String, override val attributes: Map[String,String] = Map()) extends Content(attributes)
{
  override def toString = s"\n   <content type='text' $xmlAttributes>$text</content>"

  def updated(moreAttributes: Map[String, String]) =
  {
    new ContentText(text, attributes ++ moreAttributes)
  }
}
case class ContentTextTitle( override val text: String, override val attributes: Map[String,String] = Map()) extends ContentText(text, attributes.updated("size", "title") )
case class ContentTextLarge( override val text: String, override val attributes: Map[String,String] = Map()) extends ContentText(text, attributes.updated("size", "large") )
case class ContentTextMedium(override val text: String, override val attributes: Map[String,String] = Map()) extends ContentText(text, attributes.updated("size", "medium") )
case class ContentTextSmall( override val text: String, override val attributes: Map[String,String] = Map()) extends ContentText(text, attributes.updated("size", "small") )

class ContentTextList(override val text: String, val items: List[Content], override val attributes: Map[String,String] = Map()) extends Content(attributes)
{
  override def toString = s"\n   <content compose='list' type='text' $xmlAttributes heading='$text'>${items mkString ""}</content>"

  def updated(moreAttributes: Map[String, String]) =
  {
    new ContentTextList(text, items, attributes ++ moreAttributes)
  }
}
case class ContentTextListTitle( override val text: String, override val items: List[Content], override val attributes: Map[String,String] = Map()) extends ContentTextList(text, items, attributes.updated("size", "title") )
case class ContentTextListLarge( override val text: String, override val items: List[Content], override val attributes: Map[String,String] = Map()) extends ContentTextList(text, items, attributes.updated("size", "large") )
case class ContentTextListMedium(override val text: String, override val items: List[Content], override val attributes: Map[String,String] = Map()) extends ContentTextList(text, items, attributes.updated("size", "medium") )
case class ContentTextListSmall( override val text: String, override val items: List[Content], override val attributes: Map[String,String] = Map()) extends ContentTextList(text, items, attributes.updated("size", "small") )

abstract class ContentImageList(val items: List[Content], override val attributes: Map[String,String] = Map()) extends Content(attributes)

case class ContentImageListSimple(override val items: List[Content], override val attributes: Map[String,String] = Map()) extends ContentImageList(items, attributes.updated("size", "title") )
{
  val pixelPositionOfImage = pixelPositionOfContentInTiledLayout(
    horizontalTilingHop = 960,  // Half the width of a Full HD monitor
    verticalTilingOffset = 50,
    horizontalTilingOffset = 50,
    numberOfTiles = items.length
  ) _

  var imageIndex = -1
  val images = items map {
    content =>
      imageIndex += 1
      val pos = pixelPositionOfImage(imageIndex)

      content updated Map("x" -> pos._1.toString, "y" -> pos._2.toString)
  }

  override def toString = images mkString("")

  def updated(moreAttributes: Map[String, String]) =
  {
    ContentImageListSimple(items, attributes ++ moreAttributes)
  }
}

case class ContentImageListTiled(override val items: List[Content], override val attributes: Map[String,String] = Map()) extends ContentImageList(items, attributes.updated("size", "title") )
{
  private val length: Int = items.length
  debugOn(s"number of items(n) = $length")

  var index = -1
  val images = items map {
    content =>
      index += 1
      content updated Map("tile" -> tileLayoutGovLab(length)(index).toString)
  }

  override def toString = s"\n   <content compose='tiled' type='image' $xmlAttributes>${images mkString ""}</content>"

  def updated(moreAttributes: Map[String, String]) =
  {
    ContentImageListTiled(items, attributes ++ moreAttributes)
  }
}



case class ContentImageListPositioned(override val items: List[Content], override val attributes: Map[String,String] = Map()) extends ContentImageList(items, attributes.updated("size", "title") )
{
  val pixelPositionOfImage = pixelPositionOfContentInTiledLayout(
    horizontalTilingHop = 960,  // Half the width of a Full HD monitor
    verticalTilingOffset = 50,
    horizontalTilingOffset = 50,
    numberOfTiles = items.length
  ) _

  var imageIndex = -1
  val images = items map {
    content =>
      imageIndex += 1
      val pos = pixelPositionOfImage(imageIndex)

      content updated Map("x" -> pos._1.toString, "y" -> pos._2.toString)
  }

  override def toString = s"\n   <content compose='positioned' type='image' $xmlAttributes>${images mkString ""}</content>"

  def updated(moreAttributes: Map[String, String]) =
  {
    ContentImageListPositioned(items, attributes ++ moreAttributes)
  }
}


// ------------------------------------ Referential Content

// URI based content
class ContentUri(val uri: String, override val attributes: Map[String,String] = Map()) extends Content(attributes)
{
  override def toString = s"\n      <content ref='URI' $xmlAttributes uri='$uri'/>"

  override def text: String = uri

  def updated(moreAttributes: Map[String, String]) =
  {
    new ContentUri(uri, attributes ++ moreAttributes)
  }
}

abstract class ContentUriImage(override val uri: String, override val attributes: Map[String,String] = Map()) extends ContentUri(uri,  Map("type" -> "image") ++ attributes)
case class ContentUriImagePng(override val uri: String, override val attributes: Map[String,String] = Map()) extends ContentUriImage(uri, Map("format" -> "PNG") ++ attributes)
case class ContentUriImageGif(override val uri: String, override val attributes: Map[String,String] = Map()) extends ContentUriImage(uri, Map("format" -> "GIF") ++ attributes)

case class ContentUriImageOverlay(bottom: ContentUriImage, top: ContentUriImage, override val attributes: Map[String,String] = Map()) extends Content(Map("type" -> "image") ++ attributes)
{
  override def toString = s"\n\n   <content compose='overlay' $xmlAttributes>$bottom$top\n   </content>"

  override def text: String = bottom.text + "\n" + top.text

  def updated(moreAttributes: Map[String, String]) =
  {
    ContentUriImageOverlay(bottom, top, attributes ++ moreAttributes)
  }
}

