package experimental

/**
 * @author Keith Foster
 */


import java.util.Date

import com.fasterxml.jackson.databind._
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import experimental.KVTree._
import spray.json._

object Events {
  

  // Simple Event
  trait SimpleEvent {
    def getField(key: String)
  }
  
  // Event
  abstract 
  class Event[V](val id: String, val timestamp: Date, val eventType: String)
    extends KeyValueAccess[V]

  type Events[V] = Seq[Event[V]]

  
  
  // Raw Event
  class RawEvent[V](
                          override val id:        String,
                          override val timestamp: Date,
                          override val eventType: String,
                                   val data:      String
                          )
    extends Event[V](id, timestamp, eventType)
  {
    
    // 1. Parse JSON
    
    // Spray
    val jsObject = JsonParser(data).asJsObject
    //val eventDisplay = s"jsObject = $jsObject"
    
    // Jackson
    val objectMapper = new ObjectMapper() with ScalaObjectMapper
    objectMapper.registerModule(DefaultScalaModule)
    
    type T = Map[String,Double]
    val eventMap: T = objectMapper.readValue[T](data)
    
//    val v:V = new V()
//    classOf[classOf(v)] match
    
    
    // 2. Output XML
    
    // Jackson
//    val xmlMapper = new XmlMapper();
//    val xml = xmlMapper.writeValueAsString(jsonMap); 
    
    // Scala XML Literals
//    def toXml1 = <event>
//                      <x>${jsonMap("x")}</x>
//                      <y>${jsonMap("y")}</y>
//                    </event>
                      
    // Scala String
    def toXml2 = s"""
      <event>
        <x>${eventMap("x")}</x>
        <y>${eventMap("y")}</y>
      </event>
                 """
    
    val eventDisplay = s"eventMap = $eventMap; xml = $toXml2"

    override def toString = s"RawEvent: $eventDisplay"
    
    def apply(key: String) : Option[Value[V]] =
    {
      None
      //jsObject.getFields(key).head
    }
    
    def apply(index: Int)  : Option[Value[V]] = 
    {
      None
    }
  }

  type RawEvents[V] = Seq[RawEvent[V]]

  
  
  // Concrete Event
  class ConcreteEvent[V](
                          override val id:        String,
                          override val timestamp: Date,
                          override val eventType: String,
                                   val tree:      KVTree[V]
                          )
    extends Event[V](id, timestamp, eventType)
  {
    def apply(key: String) : Option[Value[V]] = { tree(key) }
    def apply(index: Int)  : Option[Value[V]] = { tree(index) }
  }

  type ConcreteEvents[V] = Seq[ConcreteEvent[V]]

  
  
  // Static Event
  abstract 
  class StaticEvent[V](id: String, val raw: Event[V])
    extends Event[V](id, raw.timestamp, raw.eventType)
    
  type StaticEvents[V] = Seq[StaticEvent[V]]

}
