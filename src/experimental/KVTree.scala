
package experimental

import scala.util.Either
import scala.Left
import scala.Right

/**
 * Created by keith on 16/08/15.
 */
object KVTree {

  type or[L,R] = Either[L,R]   // Move this to a common module
  implicit def l2Or[L,R](l: L): L or R = Left(l)
  implicit def r2Or[L,R](r: R): L or R = Right(r)


  // Types
  type KVMap[V]  = Map[String, Value[V]]
  type KVList[V] = Vector[Value[V]]
  //type Value[V]  = V or KVTree[V]

  case class Value[V](v: V) {
    
    def toString(indent: Int = 0)
    {
      print(" " * indent + v.toString)
    }
  }

  // Key-Value Access interface
  trait KeyValueAccess[V]
  {
    def apply(key: String) : Option[Value[V]]
    def apply(index: Int) : Option[Value[V]]
  }

  // Index Access interface
  trait IndexAccess[V]
  {
    def apply(key: String) : Option[Value[V]]
    def apply(index: Int) : Option[Value[V]]
  }



    class KVTree[V](mapOrList: KVMap[V] or KVList[V]) extends KeyValueAccess[V] with IndexAccess[V] {

    def apply(key: String) : Option[Value[V]] =
    {
      mapOrList match
      {
        case Left(map) => map.get(key)
        case Right(list) =>
        {
          val index: Int = key.toInt
            Some(list(index))
        }
      }
    }

    def apply(index: Int) : Option[Value[V]] =
    {
      var result: Option[Value[V]] = None

      mapOrList match
      {
        case Left(map) => map.get(index.toString)
        case Right(list) => Some(list(index))
      }
    }
    
    
    
    // ToString
    
    def toString(indent: Int = 0)
    {
      mapOrList match
      {
        case Left(map)   => mapToString(map) 
        case Right(list) => listToString(list)
      }
      
      // Maps
      def mapToString(map: KVMap[V])
      {
        for( k: String <- map.keys; v: Value[V] <- List(map(k)))
        {
          val keyString = s"$k: "
          val keyLength = keyString.size
          
          print( " " * indent + keyString)
          
          val valueString = v.toString(indent = indent + keyLength)
          
          println(valueString)
        }
      }
      
      // Lists
      def listToString(list: KVList[V])
      {
        for( v <- list )
        {
                    
          val valueString = v.toString(indent = indent)
          
          println(valueString)
        }
      }
    }

  }

  object KVTree {
    def apply[V](map: KVMap[V]): KVTree[V] =
    {
      new KVTree[V](mapOrList = map)
    }
  }

}
