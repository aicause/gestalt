//package demonstrator.bespaced
//
///**
// * @author keith
// */
//
//import java.awt.image.BufferedImage
//
//object RasterAdapterModule
//{
//
//  // Raster Adapter : Map
//  // Based on code from http://docs.scala-lang.org/overviews/core/architecture-of-scala-collections.html
//
//  import collection.IndexedSeqLike
//  import collection.mutable.{Builder, ArrayBuffer}
//  import collection.generic.CanBuildFrom
//
//
//  abstract class RainRate2
//  case object Dry     extends RainRate2
//  case object Drizzle extends RainRate2
//  case object Wet     extends RainRate2
//  case object Storm   extends RainRate2
//
//  object RainRate2 {
//    val fromInt: Int => RainRate2 = Array(Dry, Drizzle, Wet, Storm)
//    val toInt: RainRate2 => Int = Map(Dry -> 0, Drizzle -> 1, Wet -> 2, Storm -> 3)
//  }
//
//
//  type RGBPixel = Int
//  type PixelConverter[E] = (RGBPixel) => E
//
//
//
//final class RasterAdapter[E] private (val raster: BufferedImage, val pixelConverter: PixelConverter[E])
//    extends IndexedSeq[RainRate2] with IndexedSeqLike[RainRate2, RasterAdapter[E]] {
//
//    import RasterAdapter._
//
//    // Types
//    private type Dim2 = (Int, Int)
//
//
//    // Constants
//    private val dimensions: Dim2 = (raster.getWidth, raster.getHeight)
//
//
//    // Helpers
//    private def withinBounds(coordinate: Dim2, bounds: Dim2): Boolean =
//    {
//      coordinate._1 >=0 && coordinate._1 < bounds._1 &&
//      coordinate._2 >=0 && coordinate._2 < bounds._2
//    }
//
//
//
//    // -------------------------------------------------------- Builder
//    // Mandatory re-implementation of `newBuilder` in `IndexedSeq`
//    override protected[this] def newBuilder: Builder[E, RasterAdapter[E]] =
//    {
//      RasterAdapter.newBuilder
//    }
//
//
//
//    // -------------------------------------------------------- Apply
//    // Mandatory implementation of `apply` in `IndexedSeq`
//    def apply(idx: Dim2): E =
//    {
//      if (!withinBounds(idx, dimensions)) throw new IndexOutOfBoundsException
//
//      pixelConverter(raster.getRGB(idx._1, idx._2))
//    }
//
//  }
//
//
//
//object RasterAdapter {
//    private val S = 2            // number of bits in group
//    private val M = (1 << S) - 1 // bitmask to isolate a group
//    private val N = 32 / S       // number of raster in an Int
//
//
//    def fromSeq[E](buf: Seq[E]): RasterAdapter[E] =
//    {
//      val raster = new Array[Int]((buf.numberOfTiles + N - 1) / N)
//
//      for (i <- 0 until buf.numberOfTiles)
//      {
//        raster(i / N) |= E.toInt(buf(i)) << (i % N * S)
//      }
//
//      new RasterAdapter[E](raster, buf.numberOfTiles)
//    }
//
//
//
//// We don't need to construct using varargs
////    def apply[E](seq: E*) = fromSeq(seq)
//
//
//
//    def newBuilder[E]: Builder[E, RasterAdapter[E]] = new ArrayBuffer mapResult fromSeq
//
//
//
//    implicit def canBuildFrom[E]: CanBuildFrom[RasterAdapter[E], E, RasterAdapter[E]] =
//    {
//      new CanBuildFrom[RasterAdapter[E], E, RasterAdapter[E]]
//      {
//        def apply(): Builder[E, RasterAdapter[E]] = newBuilder
//
//        def apply(from: RasterAdapter[E]): Builder[E, RasterAdapter[E]] = newBuilder
//      }
//    }
//  }
//
//
//
//
//
//  // Raster Adapter
//
////  case class RasterAdapter[E](raster: Raster) extends Matrix[Int, E]
////  {
////    type Dim2 = (Int, Int)
////
////    // Implement all the methods required for Map
////    // TODO
////
////    def +(that: this.type): this.type =
////    {
////
////    }
////
////    def -(index: Dim2): this.type =
////    {
////      val newRaster = raster.clone()
////      new RasterAdapter(newRaster)
////    }
////
////    def get(dim: Dim2): this.type =
////    {
////      val pixel = raster.getPixel(dim._1, dim._2, 0)
////    }
////
////    def iterator(): this.type =
////    {
////
////    }
////  }
//
//}
//
