package core

import java.util.{Calendar, UUID, Date}

/**
 * Created by keith on 21/09/15.
 */

trait KeyValueAccess[V]
{
  def apply(key: String) : Option[V]
  def apply(index: Int) : Option[V]
}


trait Event[V] extends KeyValueAccess[V] {
  def id: String
  def timestamp: Long
  def eventType: EventType[V,Event[V]]

  // Timestamp
  def timestampDate = new Date(timestamp)
  def timeStampCalendar =
  {
    val calendar = Calendar.getInstance(UTC)
    calendar.setTimeInMillis(timestamp)

    calendar
  }
}

object Event {

  def generateId: String = { UUID.randomUUID.toString }
}
