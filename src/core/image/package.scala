package core

/**
 * Created by keith on 16/09/15.
 */

import java.awt.Image
import java.awt.image.BufferedImage

import core._

// IO
import java.io.{File, InputStream}

// Images
import javax.imageio.ImageIO




package object image {

    type RGBPixel = Int
    type PixelConverter[E] = (RGBPixel) => E


  /**
   * Converts any Image to a BufferedImage
   *
   * @param image The Image to be converted
   * @return The resulting BufferedImage
   */
  def toBufferedImage(image: Image): BufferedImage =
  {
    image match {
      case image: BufferedImage => return image
      case _ =>
    }

    // Create a buffered image with transparency
    val result: BufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB)

    // Draw the image on to the buffered image
    val context = result.createGraphics()
    context.drawImage(image, 0, 0, null)
    context.dispose()

    result
  }
  
  
    // ------------------------------------------------------------------------------------------------- Matricies

    def convertImageToMatrix[E](pixelConverter: PixelConverter[E])(image: Any): MatrixTrait[E] =
    {
      image match
      {
        case    image: BufferedImage => new ImageAdapter(image, pixelConverter)

        case       is: InputStream   => throw new IllegalArgumentException("InputStream for image conversion not yet implemented")

        case fileName: String        => convertImageToMatrix(pixelConverter)(new File(fileName))

        case     file: File          =>
        {
          val bufferedImage = ImageIO.read(file)
          new ImageAdapter(bufferedImage, pixelConverter)
        }
      }
    }

}
