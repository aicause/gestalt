package core

/**
 * Created by keith on 7/09/15.
 */
case class Rectangle(x: Int, y: Int, width: Int, height: Int)
{
  def size = (width, height)
}
