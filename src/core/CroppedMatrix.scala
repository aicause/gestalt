package core

/**
 * Created by keith on 7/09/15.
 */
class CroppedMatrix[E](val source: MatrixTrait[E], val crop: Rectangle) extends MatrixTrait[E]
{
  private def translateFromCrop(from: Dim2): Dim2 = (from._1 + crop.x, from._2 + crop.y)
  private def translateToCrop(from: Dim2): Dim2 = (from._1 - crop.x, from._2 - crop.y)

  def apply(coordinate: Dim2): E =
  {
    source(translateFromCrop(coordinate))
  }

  def size = crop.size
}
