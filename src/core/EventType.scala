package core

/**
 * Created by keith on 21/09/15.
 */
case class EventType[V, SE](id: String, refiner: Refiner[V,SE]) {
  override def toString: String = s"Event Type $id"
}

