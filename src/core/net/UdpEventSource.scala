package core.net

import java.net.{DatagramPacket, DatagramSocket}

import core.Event

/**
 * Created by keith on 18/09/15.
 */
class UdpEventSource[V](val port: Int, val deserialiser: String => Event[V]) {

  private val sock = new DatagramSocket(port)
  private val buf = new Array[Byte](BufferSize)
  private val packet = new DatagramPacket(buf, BufferSize)

  def nextEvent(): Event[V] =
  {
    sock.receive(packet)

    // Convert to String
    val length: Int = packet.getLength
    val data = new String(packet.getData).substring(0,length)


    // Debug
    {
      println(s"received packet from: ${packet.getAddress}")
      println(s"received length     : $length")
      println(s"received data       : $data")
    }


    return deserialiser(data)
  }
}
