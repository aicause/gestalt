package demonstrator.support

import java.awt
import java.awt.Image
import java.awt.image.BufferedImage
import java.text.SimpleDateFormat
import java.util.{Locale, Calendar, TimeZone, Date}
import core.image._

import scala.util.matching.Regex.Match

import core._
import core.net._
import core.file._

import demonstrator.support.RainRate._

import demonstrator.smartspace.SmartSpaceDemo.RainRateMatrix


/**
 * Created by keith on 7/09/15.
 */


package object bom {

  // ------------------------------------------------------------------------------------------------- Types

  // Intervals
  object Interval extends Enumeration {
    type Interval = Int

    val SixMinuteInterval: Int = 6
    val OneHourInterval: Int = 60
  }

  import Interval._


  // ================================================================================================= FTP Server

  // Constants
  private val FORMAT_TIMESTAMP_BOM = new SimpleDateFormat("yyyyMMddHHmm")
  FORMAT_TIMESTAMP_BOM.setTimeZone(UTC)

  //  val FORMAT_DATETIME_BOM = "DD MMMM YYYY hh:mmaa"
//  val FORMATTER_DATETIME_BOM = new SimpleDateFormat(FORMAT_DATETIME_BOM)
//  FORMATTER_DATETIME_BOM.setTimeZone(TimeZone.getDefault)

  // Functions
  val NoTransform: PixelConverter[Any] = { x => x }



  // BOM Radar Images
  private val BOM_EXT_RADAR           = "png"

  private val BOM_SERVER_FTP          = "ftp2.bom.gov.au"
  private val BOM_DIRECTORY_FTP_RADAR = "anon/gen/radar"

  private val LOCAL_DIRECTORY_BOM     = "bom"

  // Functions
  def radarImageForFilename(now: Date)(baseFilename: String): Image =
    {
      val dayDirectory = s"${now.getYear+1900}-${now.getMonth}-${now.getDay}"
      createDirectoryOnce(s"$LOCAL_DIRECTORY_BOM", dayDirectory)

      val bomDirectory = s"$LOCAL_DIRECTORY_BOM/$dayDirectory"

      val localFileName = createLocalFileName(bomDirectory, baseFilename)

      if (playback)
      {
        debugOn(s"Read from local file system: $localFileName")

        readLocalFileImage(localFileName)
      }
      else
      {
        debugOn("Pull from FTP server...")

        val image = ftpPullImage(BOM_SERVER_FTP, BOM_DIRECTORY_FTP_RADAR)(baseFilename)

        saveLocalFileImage(localFileName, toBufferedImage(image))

        image
      }
    }


  // Largest Scale Radar Images for Melbourne
  private val baseFilename = "IDR024.T."
  // TODO: Make this generic - It would be good to be able to specify city and scale.


  // ------------------------------------------------------------------------------------------------- Radar Times

  /**
   * Get the latest BOM-FTP date-time available not later than a given date-time.
   *
   * @param filenames Names of the relevant FTP files in reverse chronological order
   * @param closeTo   The date-time for the latest possible snapshot
   * @return          The date-time matching an FTP file that exists and is close (but not later) than the closeTo date-time.
   */
  def radarTimeByActualFTPFiles(filenames: List[String], closeTo: Calendar): Calendar = {
    val hours: Int = closeTo.get(Calendar.HOUR)
    val minutes: Int = closeTo.get(Calendar.MINUTE)

    // Choose the latest and closest filename
    def extractDateTime(filename: String): Calendar = {
      val Pattern = s"$baseFilename(\\d{4})(\\d{2})(\\d{2})(\\d{2})(\\d{2}).$BOM_EXT_RADAR".r("year", "month", "dateOfMonth", "hourOfDay", "minuteOfHour")

      val resultOption: Option[Match] = Pattern findFirstMatchIn filename

      val calendar: Calendar = resultOption match {
        case Some(result: Match) => {
          val year: Int = result.group("year").toInt
          val month: Int = result.group("month").toInt
          val dateOfMonth: Int = result.group("dateOfMonth").toInt
          val hourOfDay: Int = result.group("hourOfDay").toInt
          val minuteOfHour: Int = result.group("minuteOfHour").toInt

          var cal = Calendar.getInstance(UTC)
          cal.set(year, month - 1, dateOfMonth, hourOfDay, minuteOfHour)

          cal
        }

        case None => throw new IllegalArgumentException(s"file ($filename) does not contain a date formatted correctly")
      }

      calendar
    }

    val filenameOption = filenames.find {
      filename => {
        val time: Calendar = extractDateTime(filename)
        closeTo.after(time)
      }

    }

    // Extract time from filename
    extractDateTime(filenameOption.get)
  }

  def radarTimeDelayed(interval: Interval, closeTo: Date): Date = {
    val DELAY_TO_FILE_AVAILABLE_MINUTES = 4

    val minutes: Int = closeTo.getMinutes - DELAY_TO_FILE_AVAILABLE_MINUTES
    val extraMinutes: Int = minutes % interval
    val roundedMinutes: Int = if (minutes > 5) minutes - extraMinutes else 0
    val lastRadarTime = new Date(closeTo.getTime)

    lastRadarTime.setMinutes(roundedMinutes)
    lastRadarTime.setSeconds(0)
    lastRadarTime
  }

  def radarTimePrior(interval: Interval, at: Date, back: Int): Date = {
    val hours: Int = at.getHours
    val minutes: Int = at.getMinutes - interval * back
    val normalisedMinutes: Int = if (minutes < 0) 60 + minutes else minutes
    val normalisedHours: Int = if (minutes < 0) hours - 1 else hours
    // TODO Adjust date if hours < 0

    val lastRadarTime = new Date(at.getTime)

    lastRadarTime.setHours(normalisedHours)
    lastRadarTime.setMinutes(normalisedMinutes)
    lastRadarTime.setSeconds(0)
    lastRadarTime
  }


  // ------------------------------------------------------------------------------------------------- File Construction

  // Construct Filename
  private def radarFilename(lastRadarTime: Calendar): String = radarFilename(lastRadarTime.getTime)

  private def radarFilename(lastRadarTime: Date): String = {
    val time = FORMAT_TIMESTAMP_BOM.format(lastRadarTime)
    val filename = s"$baseFilename$time.$BOM_EXT_RADAR"

    debugOn {
      val fullFilename = s"$BOM_DIRECTORY_FTP_RADAR/$baseFilename$time.$BOM_EXT_RADAR"
      val url = s"ftp://$BOM_SERVER_FTP/$fullFilename"

      println("FILENAME: " + filename)
      println("FULL FILENAME: " + fullFilename)
      println("URL: " + url)
    }

    filename
  }


  // ------------------------------------------------------------------------------------------------- Loading

  def createBomFtpUrl(filename: String): String = s"ftp://ftp2.bom.gov.au/anon/gen/radar/$filename"

  //def createLocalFile(filename: String): String = s"file:///Users/keith/Google Drive/RMIT/AICAUSE/Collaborative Engineering 2/Scala Event Specific Code/bom/radar/$filename"
  // TODO: Place recordings in a directory relative to the current directory.
  def createLocalFile(filename: String): String = s"file:///Users/keith/bom/radar/$filename"

  def latestRadarFilesMelbourne(count: Int): List[String] = { radarFileListMelbourne(new Date())(count) }

  /**
   *
   * @param count The number of snapshots you want.
   * @return      The list of (up to count) filenames that exist in the directory starting from now and going back in time. The list is in reverse chronological order.
   */
  def radarFileListMelbourne(now: Date)(count: Int): List[String] =
  {
    val filenamesWithPrefix = if ( ! playback )
      {
        ftpList(BOM_SERVER_FTP, BOM_DIRECTORY_FTP_RADAR)(prefix = baseFilename)
      }
    else
      {
        val dayDirectory = s"${now.getYear}-${now.getMonth}-${now.getDay}"
        val bomDirectory = s"$LOCAL_DIRECTORY_BOM/$dayDirectory"
        
        localList(bomDirectory)(prefix = baseFilename)
      }

    val filenamesInReverseChronologicalOrder = filenamesWithPrefix.reverse

    // Limit to the previous (count) filenames
    val filenames = filenamesInReverseChronologicalOrder.take(count)

    filenames
  }

  def rainRateMatrixList(now: Date)(filenames: List[String]): List[RainRateMatrix] =
  {
    val images = filenames map radarImageForFilename(now)

    images map RainRate.RainRate.convertRadarImageToRainRateMatrix
  }



  // ------------------------------------------------------------------------------------------------- Testing

  // Files from 27th Sep 2015
  def ftpFilesTest(closeTo: Calendar): List[String] = List(
    "IDR024.T.201509272200",
    "IDR024.T.201509272206",
    "IDR024.T.201509272212",
    "IDR024.T.201509272218",
    "IDR024.T.201509272224",
    "IDR024.T.201509272230",
    "IDR024.T.201509272236",
    "IDR024.T.201509272243",
    "IDR024.T.201509272249",
    "IDR024.T.201509272254",
    "IDR024.T.201509272300",
    "IDR024.T.201509272306",
    "IDR024.T.201509272312"
  ).reverse

  // Files from 30th Sep 2015
  def ftpFilesTest2(closeTo: Calendar): List[String] = List(
    "IDR024.T.201509300400",
    "IDR024.T.201509300406",
    "IDR024.T.201509300412",
    "IDR024.T.201509300418",
    "IDR024.T.201509300424",
    "IDR024.T.201509300429",
    "IDR024.T.201509300435",
    "IDR024.T.201509300442",
    "IDR024.T.201509300448",
    "IDR024.T.201509300454",
    "IDR024.T.201509300500",
    "IDR024.T.201509300506",
    "IDR024.T.201509300512",
    "IDR024.T.201509300518",
    "IDR024.T.201509300524",
    "IDR024.T.201509300530"
  ).reverse


  @Deprecated
  def loadRainRateMelbourne(count: Int, closeTo: Calendar): List[RainRateMatrix] = {
    loadRainRateMelbourneByFTP2(count, closeTo)
  }

  @Deprecated
  private
  def loadRainRateMelbourneByFTP2(count: Int, closeTo: Calendar): List[RainRateMatrix] = {
    val matrixList = rainRateMatrixList(10)

    matrixList
  }


  @Deprecated
  private
  def loadRainRateMelbourneByFTP1(count: Int, closeTo: Calendar): List[RainRateMatrix] = {
    //val filenames = List("IDR024.T.201509280242.png")

    // Files from 27th Sep 2015
    val filenames = ftpFilesTest2(closeTo) map {
      _ + ".png"
    }

    // TODO: Get the file list from the BOM FTP server
    {

    }

    var currentRadarTime = closeTo
    var matrixList: List[RainRateMatrix] = List()

    for (step: Int <- count.until(0, -1)) {
      debugOn(s"Step $step")

      val radarTime = radarTimeByActualFTPFiles(filenames, currentRadarTime)

      debugOn(s"actual radar time: $radarTime")

      val filename = radarFilename(radarTime)

      val image = ftpPullImage(BOM_SERVER_FTP, BOM_DIRECTORY_FTP_RADAR)(filename)

      matrixList ::= RainRate.RainRate.convertRadarImageToRainRateMatrix(image)

      currentRadarTime.add(Calendar.MINUTE, -count)
    }

    debugOn(matrixList.toString)

    matrixList
  }

  // TESTING ONLY
  @Deprecated
  def loadRainRateMelbourneByInterval(count: Int, closeTo: Date): List[RainRateMatrix] = {
    val lastRadarTime = roundRadarTime(closeTo)

    var matrixList: List[RainRateMatrix] = List()

    for (step: Int <- count.until(0, -1)) {
      debugOn(s"Step $step")

      val radarTime = radarTimePrior(SixMinuteInterval, lastRadarTime, step)

      val filename = radarFilename(lastRadarTime)

      val image = ftpPullImage(BOM_SERVER_FTP, BOM_DIRECTORY_FTP_RADAR)(filename)

      matrixList ::= RainRate.RainRate.convertRadarImageToRainRateMatrix(image)
    }

    matrixList
  }

  @Deprecated
  private
  def roundRadarTime(closeTo: Date): Date = {
    debugOn {
      println("Now: " + closeTo)
    }

    val roundedRadarTime = radarTimeDelayed(SixMinuteInterval, closeTo)

    debugOn {
      println("Rounded: " + roundedRadarTime)
    }

    roundedRadarTime
  }

  // Get the Last N Matricies
  @Deprecated
  private
  def rainRateMatrixList(count: Int): List[RainRateMatrix] = {
    val imageForFilename = ftpPullImage(BOM_SERVER_FTP, BOM_DIRECTORY_FTP_RADAR) _

    val filenamesWithPrefix = ftpList(BOM_SERVER_FTP, BOM_DIRECTORY_FTP_RADAR)(prefix = baseFilename)
    val filenamesInReverseChronologicalOrder = filenamesWithPrefix.reverse

    // Limit to the previous (count) filenames
    val filenames = filenamesInReverseChronologicalOrder.take(count)

    val images = filenames map imageForFilename

    images map RainRate.RainRate.convertRadarImageToRainRateMatrix
  }
  
}

