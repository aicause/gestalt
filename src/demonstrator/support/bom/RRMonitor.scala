package demonstrator.support.bom

import java.awt.Image
import java.util.Date

// Gestalt
import core._


import demonstrator.support



/**
 * Created by keith on 16/10/15.
 */

object RRMonitor {

  // Convenience Constructors
  def apply[INFO](
                   //pixelConverter: PixelConverter[INFO] = NoTransform,
                   transformer: Image => INFO,
                   processor: INFO => Unit
                   ) = new RRMonitor(transformer, processor)


}

// TODO: Generify the RR and UV Monitors to take five parameters: poller, time-checker, loader, transformer and processor.
// TODO: A generic monitor should not have knowledge of the pixels/images and matricies.

// HACK: For the moment just hard code both monitor instances.

class RRMonitor[INFO](
                       //pixelConverter: PixelConverter[INFO] = NoTransform ,
                       transformer: Image => INFO,
                       processor: INFO => Unit
                       ) {


  // -------------------------------------------------------------------------------------------- Background Task

  // single threaded execution context
  //implicit val context = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

  val r: Runnable = new Runnable {

    override def run(): Unit =
    {
      val SECONDS = 1000L

      while (true)
      {
        println("Running RR Monitor asynchronously on another thread")

        val time = support.playbackOrLiveTime
        val now: Date = new Date(time)

        // Poll
        val fileList = radarFileListMelbourne(now)(1)

        debugOn(s"RR fileList = $fileList")

        if (fileList.nonEmpty)
        {
          val baseFilename = fileList.head

          debugOn (s"RR base filename: $baseFilename")

          // Load
          val radarImage = radarImageForFilename(now)(baseFilename)

          if (radarImage == null)
          {
            log("Can't get radar image", s"Filename: $baseFilename")
          }
          else
          {
            // Transform
            val matrix = transformer(radarImage)

            // Process
            processor(matrix)

            debugOn(
              s"Processed $baseFilename"//,
              //s"matrix: ${matrix.summary}"
            )
          }
        }

        Thread.sleep(180 * SECONDS)
      }
    }
  }

  val thread = new Thread(r, "RR Monitor")
  thread.setDaemon(true)
  thread.start()

}