package demonstrator

import java.util.Date

/**
 * Created by keith on 20/11/15.
 */
package object support {

  // Playback / Live Mode
  var startTime: Long = new Date().getTime // DON"T CHANGE THIS

  // To change to playback mode
  // 1. set playback to true
  // 2. set timeZero to the startTime subtract some days/hours or an absolute time/dat

  // CONFIG: Live Demo
  //var playback = false
  //var timeZero: Long = startTime - (140 *1000)

  // CONFIG: Archive from 27th August 2015
  var playback = true
  var timeZero: Long = new Date(2015-1900-1, 12-1, 28, 11, 4).getTime

  def playbackOrLiveTime: Long =
  {
    val realTime: Long = new Date().getTime
    val time: Long =
      if (support.playback) {
        val relativeTime = realTime - support.startTime

        support.timeZero + relativeTime
      }
      else
      {
        realTime
      }

    time
  }

}
