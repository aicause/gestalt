/**
 * Created by keith on 29/07/15.
 */

package demonstrator.udp

import java.net.{DatagramPacket, DatagramSocket}

object UdpEchoServer
{
  val BufferSize = 16
  val Port = 4444

  def main(args : Array[String]) : Unit =
  {
    println("UDP Echo Server Started...")

    val sock = new DatagramSocket(Port)
    val buf = new Array[Byte](BufferSize)
    val packet = new DatagramPacket(buf, BufferSize)

    while (true)
    {
      sock.receive(packet)

      println(s"received packet from: ${packet.getAddress}")

      sock.send(packet)

      //println(s"echoed data (first 16 bytes): ${packet.getData.take(16).toString}")

      val length: Int = packet.getLength
      println(s"Length : $length")

      //val header: Array[Byte] = packet.getData.take(24)
      println(s"echoed data (first 100 bytes): ${new String(packet.getData).substring(0,length)}")
    }
  }
}
