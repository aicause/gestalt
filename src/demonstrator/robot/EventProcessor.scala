package demonstrator.robot

/**
 * @author keith
 */

// UDP
import java.net.{DatagramSocket, DatagramPacket, InetAddress}

// Jackson
import com.fasterxml.jackson.databind._
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.DefaultRequiredAnnotationIntrospector
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper



object EventProcessor
{
  
  // ------------------------------------- Constants
  
  // Receiving
  val BufferSize = 1024
  val ReceivePort = 3333
  
  // Sending
  val LocalTest = (InetAddress.getByName("127.0.0.1"), 5555)
  val Sage = (InetAddress.getByName("10.234.2.220"), 5555)

  // Note: variable names must start with a lower case letter for  multiple assignment to work - go figure!
  val (sendAddress, sendPort) = Sage

  
  
  // ------------------------------------- Main (event loop)
  
  def main(args : Array[String]) : Unit =
  {
    println("Robot Event Processor Started...")

    val sock = new DatagramSocket(ReceivePort)
    val buf = new Array[Byte](BufferSize)
    val packet = new DatagramPacket(buf, BufferSize)

    while (true)
    {
      sock.receive(packet)

      // Convert to String
      val length: Int = packet.getLength
      val data = new String(packet.getData).substring(0,length)
      
      
      // Debug
      {
        println(s"received packet from: ${packet.getAddress}")
        println(s"received length     : $length")
        println(s"received data       : $data")
      }
      
      
      // Process
      {
        processJsonToXmlOverUdp(data)
      }
      
    }
  }
  
  
  
  // ------------------------------------- Process JSON
  
  def processJsonToXmlOverUdp(rawJson: String)
  {
    val eventMap = parseJsonToMapToString(rawJson)
    val eventXml = serialiseEventMapToXml(eventMap)
        
    sendToWellKnownUdpServer(sendAddress, sendPort, eventXml)
  }
  
  
  
  // ------------------------------------- Parse JSON to Map[String,String]
  
  //type T = Either[String, Int]       // Can not deserialize instance of scala.util.Either out of VALUE_STRING token
  type T = Object
  
  def parseJsonToMapToString(rawJson: String): Map[String,T] =
  {
    // Parse the JSON into a Map
    val objectMapper = new ObjectMapper() with ScalaObjectMapper
    objectMapper.registerModule(DefaultScalaModule)
    
    type MT = Map[String, T]
    val eventMap = objectMapper.readValue[MT](rawJson)

    return eventMap
  }
  
  
  
  // ------------------------------------- Serialise EventMap to XML
  
  def serialiseEventMapToXml(eventMap: Map[String, T]): String =
  {
      def entries: String = 
      {
        
        val elements = eventMap map 
        { 
          pair =>
            
          val key = pair._1
          val value = pair._2
          
          s"<$key>$value</$key>\n"
        }
        
        val result: String = elements reduce { _ + _ }
        
        return result
      }
      
      
      val basicTest = s"""
      <event>
        $entries
      </event>
      """
        
     val wallTest = "<output>" + 
       // modifying output data in lieu of jar file modified test case
//       "<command device='govlab' type='display' profile='machineprofile1'></command>" +
       "<command device='miniwall' type='display' profile='alice'></command>" +
       s"$entries" +
//       "<command device='miniwall' type='display' profile='charles'></command>" +
//       "<command device='incrc' type='event' catagory='correctcoverage' id='1001'></command>" +
//       "<command device='auabb' type='view' image='substation9.jpg' rectx='300' recty='500' rectw='150' recth='100' text='hello world' txtx='100' txty='50'></command>" +
//       "<command device='riotinto-pilbara' type='map' lat='-37.800114' long='144.9671113' zoom='15z'></command>" +
//       "<command device='riotinto-mel' type='earth' lat='-37.800114' long='144.9671113' height='100m'></command>" +
//       "<command device='socialmedia' type='tweeter|web' msg='node1'></command>" +
//       s"<node1> $eventInfo </node1>" +
       "</output>"

       
      // Commands
      val aliceLogin = "<command device='govlab' type='display' profile='alice'></command>"
      val hazelPowerPlantMap = "<command device='govlab' type='earth' lat='-37.800114' long='144.9671113' height='100m'></command>"
      
       
      // SWITCH
      val command = hazelPowerPlantMap
       
      val output = s"<output>$command$entries</output>"
       
      return output
  }
  
  
  
  // ------------------------------------- Send to Well Known UDP Server
  
  def sendToWellKnownUdpServer(sendAddress: InetAddress, sendPort: Int, data: String)
  {
    val length = data.length
    
    // Debug
    {
      println(s"sending to address: $sendAddress:$sendPort")
      println(s"sendlength        : $sendAddress:$length")
      println(s"send data         : $sendAddress:$data")      
    }
    
    
    // Send over UDP
    val sock = new DatagramSocket()
    
    val sendData = data.getBytes()
    val sendPacket = new DatagramPacket(
        sendData,
        sendData.length,
        sendAddress,
        sendPort
        )
    
    sock.send(sendPacket)  
  }
  
  
}







