package demonstrator.smartspace

import core._
import crave._
import crave.alarms.AlarmHigh

/**
 * Created by keith on 18/09/15.
 */



case class AlarmPowerOutage(override val contributors: List[String] = List(), override val rootCause: String = "Unknown")
                           (override val relevantContent: List[Content])
                           (location: GeoLocation)
                           (device: Device)
                           (target: String)
                           extends AlarmHigh(summary = "Power Outage")(contributors)(rootCause)(relevantContent = List())(location)(device)(target)
