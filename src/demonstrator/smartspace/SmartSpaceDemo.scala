/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package demonstrator.smartspace

/**
 * @author keith
 */

import java.awt.Image
import java.util.{GregorianCalendar, Date, Calendar}

import bespaced.Examples.BespacedSerialisation
import core.image._
import demonstrator.support
import experimental.bespaced.Before

import scala.concurrent.duration._

import bespaced._

// Jackson
import com.fasterxml.jackson.databind._
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.DefaultRequiredAnnotationIntrospector
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper

// Gestalt
import core._
import core.net._

// Crave
import _root_.crave._

// UDP
import java.net.{DatagramSocket, DatagramPacket, InetAddress}

// BeSpaceD
import BeSpaceDCore._

// Support
import demonstrator.support._
import demonstrator.support.bom._
import demonstrator.support.bom.BOMSamples._

import demonstrator.support.geo.Locations._

import demonstrator.support.RainRate.{Dry, RainRate}

import demonstrator.support.RainRate.RainRate.rainRatePixelConverter
import demonstrator.support.arpansa.UVMonitor

// Model
import demonstrator.smartspace.model.SmartSpaceModel._





object SmartSpaceDemo
{
  // Types
  type PowerValue = Any
  type PowerEvent = core.Event[PowerValue]
  type RainRateMatrix = MatrixTrait[RainRate]


  // Constants
  val locationOfPowerPlant = OwnBox("Power Plant", 100, 100, 120, 120)
  val PowerEventID: Device = "aicause.power"
  val PowerEventType = EventType[PowerValue, PowerEvent](PowerEventID, IdentityRefiner)

  // Functions
  def wet(rainRate: RainRate ): Boolean =
  {
    rainRate != Dry
  }


  // ------------------------------------- Background tasks

  // UV Index Monitoring
  val uvMonitor = UVMonitor[UVIndex]("melbourne", { uvcity => uvcity.index }, insertUVIndex)

  def insertUVIndex(uvIndex: UVIndex) =
  {
    val snapshotTime = new Date(support.playbackOrLiveTime)

    bsd_history = historyUpdatedWithUV_bsd(bsd_history)(snapshotTime, uvIndex)
  }

  /**
   * Adds the given UV data from ARPANSA into the history making sure not to duplicate data for the same time.
   *
   * @param bsd_history the BeSpaceD Invariant that represents the current history of facts.
   * @param currentTime the time this snapshot is related to.
   * @param index The latest UV data from ARPANSA that may be already recorded.
   * @return The new updated history including information extracted from the snapshot.
   */
  def historyUpdatedWithUV_bsd(bsd_history: Invariant)(currentTime: Date, index: UVIndex): Invariant =
  {
    val implication = IMPLIES(TimePoint(currentTime), ComponentState(index))

    // Check to see if we have applied this snapshot before
    val foundFirst = foundImplication(bsd_history, implication)
    val redundant = foundFirst.isDecided && foundFirst.getOption.get.isDefined

    // Store this UVData into BeSpaceD history.
    if ( ! redundant )
    {
      debugOn(s"HISTORY: Add Snapshot @ $currentTime")

      val oldImplicationsFlat = unwrapAnds(bsd_history)
      val newImplicationsFlat = unwrapAnds(implication)

      debugOn(s"HISTORY: oldSize = ${oldImplicationsFlat.length}; newSize = ${newImplicationsFlat.length}")

      BIGAND(newImplicationsFlat ++ oldImplicationsFlat)
      // TODO: Enhance BeSpaceD to have AND() as an companion apply() method then use type matching to rewrite nested ANDs into one BIGAND.
    }
    else
    {
      bsd_history
    }
  }

  // -------------------------------------------------------------------------------------------- RR Processing

  // Rain Rate Monitoring
  val rrMonitor = RRMonitor[RainRateMatrix](convertImageToMatrix(rainRatePixelConverter), insertRainRate)

  val imageToRainRateMatrix: Image => RainRateMatrix = convertImageToMatrix(rainRatePixelConverter)

  def insertRainRate(info: RainRateMatrix)
  {
    val snapshotTime = new Date(support.playbackOrLiveTime)

    bsd_history = historyUpdatedWithRR_bsd(bsd_history)(snapshotTime, info)
  }

  /**
   * Adds the given RR data from BOM into the history making sure not to duplicate data for the same time.
   *
   * @param bsd_history the BeSpaceD Invariant that represents the current history of facts.
   * @param currentTime the time this snapshot is related to.
   * @param snapshot The latest RR image data from BOM that may be data already recorded for some or all cities.
   * @return The new updated history including information extracted from the snapshot.
   */
  def historyUpdatedWithRR_bsd(bsd_history: Invariant)(currentTime: Date, snapshot: RainRateMatrix): Invariant =
  {
    val firstLocation: Dim2 = (0, 0)
    val firstInfo = snapshot(firstLocation)

    val firstImplication = IMPLIES(TimePoint(currentTime), ComponentState(firstInfo))

    // Check to see if we have applied this snapshot before
    val foundFirst = foundImplication(bsd_history, firstImplication)
    val redundant = foundFirst.isDecided && foundFirst.getOption.get.isDefined

    // Store this UVData into BeSpaceD history.
    if ( ! redundant )
    {
      debugOn(s"HISTORY: Add Snapshot @ $currentTime")
      val newImplications = for ( (dim: Dim2, info: RainRateMatrix) <- snapshot.toList) yield
      {
        val implication = IMPLIES(TimePoint(currentTime), ComponentState(info))

        implication
      }

      val oldImplicationsFlat = unwrapAnds(bsd_history)
      val newImplicationsFlat = newImplications flatMap unwrapAnds

      debugOn(s"HISTORY: oldSize = ${oldImplicationsFlat.length}; newSize = ${newImplicationsFlat.length}")

      BIGAND(newImplicationsFlat ++ oldImplicationsFlat)
      // TODO: Enhance BeSpaceD to have AND() as an companion apply() method then use type matching to rewrite nested ANDs into one BIGAND.
    }
    else
    {
      bsd_history
    }
  }





  // ------------------------------------- Main (event loop)
  
  def main(args : Array[String]): Unit =
  {
    // Capture some weather data in BeSpaceD
    import BeSpaceDData._
    
    
    
    val matrix = getRainRateDataDummy
    val spacialData = spacialDataFromMatrix[RainRate](_ => true, matrix, 0)
    val invariant = BIGAND(spacialData)
    println("invariant:")
    println(invariant)
    save(invariant, "aicause.weather.samplePrecipitationRadar")

    System.exit(0)
    
    testOn
    {
      println("TEST BeSpaceD Serialised Data (via standard Java Serialisation)")
      println(BespacedSerialisation.serialisedData)
      println("\nSerialise 2")
      println(BespacedSerialisation.serialisedData2)
      println("\nSerialise 500")
      println(BespacedSerialisation.serialisedData500)
      println("\n\n")
    }
    testOn
    {
      val testEvent: RawEvent[Any] = RawEvent(PowerEventType, Map("type" -> "Testing Only"))
      val list = getLatestRainRateData(testEvent)

      var i = 1
      list.foreach
      {
        matrix: RainRateMatrix =>
          println()
          println(i); i += 1
          println(matrix.crop(CroppingNiceCloudedRegionOn5Sep))
      }

      val timeSeries = SpatioTemporalDataFromMatrix[RainRate](wet, list)

      testOn(s"timeSeries length is ${timeSeries.length}")
      testOff
      {
        for ((matrix: MatrixTrait[RainRate], index: Int) <- list.zipWithIndex) {
          val spacialData = spacialDataFromMatrix[RainRate](_ => true, matrix, index)

          println(s"spacialData length is ${spacialData.length}")
        }
      }
    }




    // -------------------------------------------------- Rules
    var powerPlantRules = List[Invariant] (
      Before(1 hour, OccupyBox(1,1,1,1)),
      Before(1 hour, OccupyBox(1,1,1,1))
    )


    def data2PowerEvent(data: String): PowerEvent =
      {
        val dataMap: Map[String, PowerValue] = parseJsonToMap(data)
        RawEvent(PowerEventType, dataMap)
      }

    // EVENT LOOP
    println("PowerOutageDemo started ...")

    val eventSource = new UdpEventSource(ReceivePort, data2PowerEvent)

    println("Waiting for UDP packet. Type \"nc -u localhost 4444\" into terminal.")
    
    while (true)
    {
      val event = eventSource.nextEvent()

      println("event.timestamp: " + event.timestamp)

      // Process
      {
        //processPowerOutageEventTest1(event)
        processPowerOutageEventTest2(event)
      }
    }
  }



  // ------------------------------------- Process JSON

  def processJsonToXmlOverUdp(rawJson: String)
  {
    val eventMap = parseJsonToMap(rawJson)
    val eventXml = serialiseToXml(eventMap = eventMap)

    sendUdp(sendAddress, sendPort, eventXml)
  }



  // ------------------------------------- PREPARE input data for BeSpaceD

  // TESTING ONLY - Dummy input data
  def getRainRateDataDummy: RainRateMatrix =
  {
    // Note: This is the date of the sample that was downloaded for testing purposes
    val now = new GregorianCalendar(2015, Calendar.AUGUST, 26).getTime

    // Load Sample BOM data into matrix
    val matrix = bom.loadRainRateMelbourneByInterval(1, closeTo = now)

    // Crop the first matrix
    val croppedMatrix = matrix.head.crop( BOMSamples.CroppingNiceCloudedRegionOn26Aug )

    // and print it for testing purposes.
    println(croppedMatrix)

    return croppedMatrix
  }


  @Deprecated
  def getLatestRainRateDataTest(event: PowerEvent): List[RainRateMatrix] =
  {
    bom.loadRainRateMelbourneByInterval(10, closeTo = event.timestampDate)
  }

  @Deprecated
  def getLatestRainRateData(event: PowerEvent): List[RainRateMatrix] =
  {
    bom.loadRainRateMelbourne(10, closeTo = event.timeStampCalendar)
  }

  def getLatestRainRateFiles(event: PowerEvent): List[String] =
  {
    bom.latestRadarFilesMelbourne(10)
  }



  // ------------------------------------- PROCESS the event using BeSpaceD

  import demonstrator.support.crave._


  def spacialDataFromMatrix[E](occupied: E => Boolean, matrix: MatrixTrait[E], timePoint: Int): List[OccupyPoint] =
  {
    val list: List[OccupyPoint] =
      for( (coordinate: (Int, Int), element: E) <- matrix.toList; if occupied(element) ) yield
      {
        OccupyPoint.apply(coordinate._1, coordinate._2)
      }

    debugOff(s"spacial list length is ${list.length}")

    list
  }

  def SpatioTemporalDataFromMatrix[E](occupied: E => Boolean, series: List[MatrixTrait[E]]): List[Invariant] =
  {
    val zipWithIndex: List[(MatrixTrait[E], Int)] = series.zipWithIndex

    debugOff(s"zipWithIndex length is ${zipWithIndex.length}")
    debugOff
    {
      for ((matrix, index) <- zipWithIndex)
      {
        val average: Double = matrix.average({ x: E => if (occupied(x)) 1 else 0 })
        val percentage: Double = Math.round(average * 10000) / 100

        println(s"matrix @$index) average cloud cover is $percentage%")
      }
    }

    for ((matrix, index) <- zipWithIndex; occupiedPoint: OccupyPoint <- spacialDataFromMatrix[E](occupied, matrix, index)) yield
      {
        val t = index + 1

        debugOff(s"matrix $t, point ${(occupiedPoint.x, occupiedPoint.y)}}) rain rate is ${matrix(occupiedPoint.x, occupiedPoint.y)}")

        //val t = timePoint(matrix)
        IMPLIES(
          AND(
            TimePoint(t),
            Owner("Cloud")
          ),
          occupiedPoint
        )
      }
  }



  def processPowerOutageEventTest2(event: PowerEvent) =
  {
    // Hard code the target of the message.
    val target = "Jan Olaf Blech"

    val content: List[Nothing] = List()

    // ---------------------------------- Generate First message
    {
      val alarm1 = AlarmPowerOutage(List())(relevantContent = content)(location = HazelPowerPlant)(DeviceGovLab)(target)

      //val outputXml1 = serialiseToXml(alarm1, eventMap)
      val outputXml1 = serialiseToXml(alarm1, Map[String,Any]())

      sendUdp(sendAddress, sendPort, outputXml1)
    }


    // ---------------------------------- Do Root Cause Analysis
    val now = new Date()   // Ignore playback mode because this is a live event processing.

    // Get more information from the BOM Radar images
    val baseFilenames  = latestRadarFilesMelbourne(10)
    val images     = baseFilenames map bom.radarImageForFilename(now)
    val matricies  = images map RainRate.convertRadarImageToRainRateMatrix

    // Get more information from the ARPANSA UV Index
    val uvIndexNowMelbourne = arpansa.latestUvIndex(now)("melbourne")

    testOn(s"uvIndexNowMelbourne = $uvIndexNowMelbourne")



    // ---------------------------------- Generate Snapshot data and add to history




    // ---------------------------------- Generate Projected data and add to future

    // Determine how much future we need
    // (work backwards from furthest time point in the future mentioned in a query)
    //val endTime = furthestTime(SmartSpaceModel.rules)


    // ---------------------------------- Place info into BeSpaceD
    val precipitationOverLastHour = SpatioTemporalDataFromMatrix[RainRate](wet, matricies)

    // TODO: Process the BeSpaceD data
    
    val numberOfInvariants = precipitationOverLastHour.length

    
    
    // ---------------------------------- Query BeSpaceD for something Smart
    
    val cdefs = new BeSpaceDCore.CoreDefinitions
    val boxOccupied = cdefs.collisionTestsBig(precipitationOverLastHour.map { case IMPLIES(_,p) => p }, cdefs.unfoldInvariant(OccupyBox(100,100,101,102))::Nil)
    
    testOn(s"box is occupied : $boxOccupied")
    
    
    
    // ---------------------------------- Generate 2nd message
    {
      // Content

      val contributors = List("A storm may be over the power plant.")

      val info1 = ContentTextMedium(s"The power plant is ${if (!boxOccupied) "not " }covered by cloud.")
      val info2 = ContentTextMedium(s"The Photo Voltaic Strength over the Melbourne area right now is $uvIndexNowMelbourne.")
      val info3 = ContentTextMedium(s"Number of Invariants in BeSpaceD is $numberOfInvariants.")

      val percentage: Double = Math.round(numberOfInvariants.toDouble / (512 * 512 * 10).toDouble * 10000) / 100.0
      val info4 = ContentTextMedium(s"This indicates $percentage% of all possible points are occupied.")

      val mapContent = ContentUriImageGif(createBomFtpUrl("IDR024.gif"))

      // Create Image Overlay contents for all radar images used (up to 10)
      val radarContents: List[Content] = baseFilenames map {
        baseFilename =>
          val rainRateImage = ContentUriImagePng(createBomFtpUrl(baseFilename))

          //ContentUriImageOverlay(bottom = mapContent, top = rainRateImage)
          // TODO: IMPORTANT Put the above code back in after the ABB demo
          // HACK: For old XML format for Lasith in ABB demonstration
          rainRateImage
      } reverse

      //val relevance = info1 :: info2 :: info3 :: info4 :: radarContents
      // TODO: IMPORTANT Put the above code back in after the ABB demo
      // HACK: For old XML format for Lasith in ABB demonstration


      // TODO: Add w,h into all images
      debugOn(s"radarContents = $radarContents")

      val pixelPositionOfImage = pixelPositionOfContentInTiledLayout(
        horizontalTilingHop = 960,  // Half the width of a Full HD monitor
        verticalTilingOffset = 150,
        horizontalTilingOffset = 150,
        numberOfTiles = radarContents.length
      ) _

      var imageIndex = -1
      val relevance = radarContents map {
        content =>
          imageIndex += 1
          val pos = pixelPositionOfImage(imageIndex)

          content updated Map("x" -> pos._1.toString, "y" -> pos._2.toString)
      }



      val alarm2 = AlarmPowerOutage(contributors = contributors)(relevantContent = relevance)(location = HazelPowerPlant)(DeviceGovLab)(target)



      // Presentations



      val outputXml2 = serialiseToXml(alarm2, Map[String,Any]())

      sendUdp(sendAddress, sendPort, outputXml2)
    }

  }



  @Deprecated
  def processPowerOutageEventTest1(event: PowerEvent) =
  {
    val matrix = getRainRateDataDummy

    println(matrix)

    //val eventMap = parseJsonToMap(data)

    val jan = "Jan Olaf Blech"

    // Generate First message
    val alarm1 = AlarmPowerOutage(List())(relevantContent = List())(location = HazelPowerPlant)(DeviceGovLab)(jan)

    //val outputXml1 = serialiseToXml(alarm1, eventMap)
    val outputXml1 = serialiseToXml(alarm1, Map[String,Any]())

    sendUdp(sendAddress, sendPort, outputXml1)

    // Sleep
    Thread.sleep(5000)

    // Generate 2nd message
    val contributors = List("A storm is over the power plant.")
    val mapContent = ContentUriImagePng("ftp://ftp2.bom.gov.au/anon/gen/radar")
    val radarContent = ContentUriImagePng("ftp://ftp2.bom.gov.au/anon/gen/radar")
    val relevance = List(mapContent, radarContent)
    val alarm2 = AlarmPowerOutage(contributors = contributors)(relevantContent = relevance)(location = HazelPowerPlant)(DeviceGovLab)(jan)

    //val outputXml2 = serialiseToXml(alarm2, eventMap)
    val outputXml2 = serialiseToXml(alarm2, Map[String,Any]())

    sendUdp(sendAddress, sendPort, outputXml2)
  }
}







